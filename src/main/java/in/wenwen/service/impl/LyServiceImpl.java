package in.wenwen.service.impl;

import java.util.List;

import in.wenwen.dto.LyDto;
import in.wenwen.entity.Article;
import in.wenwen.entity.Ly;
import in.wenwen.service.IIpService;
import in.wenwen.service.ILyService;
import in.wenwen.util.EmailUtil;
import in.wenwen.util.UserManage;

import org.jiucheng.aop.Aop;
import org.jiucheng.ioc.annotation.Inject;
import org.jiucheng.ioc.annotation.Service;
import org.jiucheng.orm.SQLHelper;
import org.jiucheng.orm.interceptor.Close;
import org.jiucheng.orm.interceptor.Tx;
import org.jiucheng.plugin.db.BaseServiceImpl;
import org.jiucheng.util.IpUtil;
import org.jiucheng.util.ObjectUtil;
import org.jiucheng.util.StringUtil;
import org.jiucheng.web.util.WebUtil;

@Service("lyService")
@Aop(Close.class)
public class LyServiceImpl extends BaseServiceImpl implements ILyService {
    
    @Inject
    private IIpService ipService;
    
    @Aop(Tx.class)
    public void saveLyUpdateArticle(Ly ly, Article article) {
        String title = article.getTitle();
        Long assess = article.getAssess();
        article = new Article();
        article.setId(ly.getArticleId());
        article.setAssess(assess + 1);
        article.setModifyDatetime(UserManage.getReceiveDatetime());
        update(article);
        
        ly.setWebappId(UserManage.getWebappId());
        ly.setCreateDatetime(UserManage.getReceiveDatetime());
        ly.setModifyDatetime(UserManage.getReceiveDatetime());
        ly.setImg(getImg());
        String ip = WebUtil.getIp();
        if(IpUtil.isIp(ip)) {
            ly.setIp(ip);
            ly.setIpName(ipService.getName(IpUtil.toLong(ip)));
        }
        Long longId = (Long) save(ly);
        
        // 如果是回复其他留言则邮件通知
        Long lyId = ly.getLyId();
        if(ObjectUtil.isNull(lyId)) {
            return;
        }
        Ly needCall = get(Ly.class, lyId);
        if(ObjectUtil.isNull(needCall) || !UserManage.getWebappId().equals(needCall.getWebappId())) {
            return;
        }
        if(StringUtil.isNotBlank(needCall.getEmail())) {
            String subject = "《" + title + "》" + ly.getAuthor() + "回复了你，" + UserManage.getWebappName();
            String msg = "<html>";
            String url = UserManage.getWebappUrl() + "article/" + article.getId() + "#cmt" + longId;
            msg += "<h3>" + "<a href=\"" + url + "\">《" + title + "》</a>" + "</h3>";
            msg += "<p>回复内容：" + ly.getContent() + "</p>";
            msg += "<p>回复昵称：" + ly.getAuthor() + "</p>";
            msg += "<p>文章地址：<a href=\"" + url + "\">" + url + "</a></p>";
            msg += "</html>";
            EmailUtil.sendHtml(needCall.getEmail(), subject, msg);
        }
        // \如果是回复其他留言则邮件通知
    }
    
    private String getImg() {
        double d = (Math.random() * 100);
        d += 1;
        return Integer.toString((int)d).concat(".jpg");
    }

    public List<Ly> getNew(Long webappid, Long size) {
        SQLHelper sh = new SQLHelper("SELECT a.content,a.article_id FROM ly a WHERE a.webapp_id = ? ORDER BY id DESC LIMIT ?");
        sh.insertValue(webappid);
        sh.insertValue(size);
        return getBaseDao().listBySQL(Ly.class, sh);
    }

    public List<LyDto> getArticleLy(Long webappId, Long articleId, Long lyId) {
        SQLHelper sh = new SQLHelper("SELECT a.id,a.author,a.url,a.img,IFNULL(a.ip_name,'未知') ip_name, a.content, a.article_id,DATE_FORMAT(a.create_datetime, '%y-%m-%d %k:%i') AS create_datetime FROM ly a WHERE a.webapp_id = ? AND a.article_id = ? ");
        sh.insertValue(webappId);
        sh.insertValue(articleId);
        if(ObjectUtil.isNotNull(lyId)) {
            sh.append(" AND a.ly_id = ? ");
            sh.insertValue(lyId);
        }else {
            sh.append(" AND a.ly_id IS NULL ");
        }
        List<LyDto> lys = getBaseDao().listBySQL(LyDto.class, sh);
        for(LyDto ly : lys) {
            ly.setLys(getArticleLy(webappId, articleId, ly.getId()));
        }
        return lys;
    }

}
