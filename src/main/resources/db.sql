DROP TABLE IF EXISTS `sso`;
CREATE TABLE `sso`(
    `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
    `user_id` BIGINT(16) NOT NULL,
    `token` VARCHAR(32) NOT NULL,
    `create_datetime` DATETIME,
    `modify_datetime` DATETIME,
    PRIMARY KEY(`id`),
    KEY `index_0`(`token`)
)ENGINE=InnoDB DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `webapp`;
CREATE TABLE `webapp` (
  `id` bigint(16) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `is_halt` varchar(2) DEFAULT 'F',
  `create_datetime` datetime DEFAULT NULL,
  `modify_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `webapp`(`name`) VALUE('主站');

DROP TABLE IF EXISTS `domain`;
CREATE TABLE `domain` (
  `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
  `webapp_id` BIGINT(16) DEFAULT NULL,
  `site` VARCHAR(128) NOT NULL,
  `main` VARCHAR(256) DEFAULT '/index' COMMENT '默认主页',
  `templet` VARCHAR(64) NOT NULL DEFAULT 'default',
  `is_halt` VARCHAR(2) DEFAULT 'F',
  `create_datetime` DATETIME DEFAULT NULL,
  `modify_datetime` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_0` (`site`)
)ENGINE=INNODB DEFAULT CHARSET=utf8;
INSERT INTO `domain`(`webapp_id`,`site`,`templet`) VALUE(1,'localhost','default');
INSERT INTO `domain`(`webapp_id`,`site`,`templet`) VALUE(1,'127.0.0.1','default');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`(
    `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
    -- `webapp_uid` VARCHAR(32) NOT NULL,
    `uid` VARCHAR(64) NOT NULL,
    `nick` VARCHAR(128) DEFAULT '',
    `password` VARCHAR(32) NOT NULL,
    `create_ip` VARCHAR(16),
    `modify_ip` VARCHAR(16), 
    `create_datetime` DATETIME,
    `modify_datetime` DATETIME,
    `is_halt` VARCHAR(2) DEFAULT 'F',
    `email` VARCHAR(128),
    `mobile` VARCHAR(32),
    `sex` VARCHAR(2) DEFAULT 'G',
    PRIMARY KEY (`id`),
    UNIQUE KEY `index_0`(`webapp_id`,`uid`)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user_auth`;
CREATE TABLE `user_auth`(
    `id` BIGINT(16) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `webapp_id` BIGINT(16) NOT NULL,
    `uri` VARCHAR(128) NOT NULL COMMENT 'uri',
    `descript` VARCHAR(56),
    `descript_en` VARCHAR(56),
    `is_halt` VARCHAR(2) NOT NULL DEFAULT 'F' COMMENT '是否停用',
    `create_datetime` DATETIME,
    `modify_datetime` DATETIME,
    PRIMARY KEY (`id`),
    UNIQUE KEY `index_0`(`webapp_id`,`uri`)
);

DROP TABLE IF EXISTS `user_auth_cache`;
CREATE TABLE `user_auth_cache`(
    `id` BIGINT(16) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `webapp_id` BIGINT(16) NOT NULL,
    `user_id` BIGINT(16),
    `cache_string` VARCHAR(256),
    `is_halt` VARCHAR(2) NOT NULL DEFAULT 'F' COMMENT '是否停用',
    `create_datetime` DATETIME,
    `modify_datetime` DATETIME,
    PRIMARY KEY (`id`),
    KEY `index_0`(`webapp_id`,`user_id`)
);

DROP IF EXISTS `upload_file`;
CREATE TABLE `upload_file` (
  `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
  `file_type` VARCHAR(20) NOT NULL DEFAULT 'ATTACHMENT' COMMENT 'PHOTO,SIGN,ATTACHMENT',
  `dir_code` VARCHAR(2) NOT NULL DEFAULT 'A',
  `dir` VARCHAR(16) NOT NULL,
  `name` VARCHAR(32) NOT NULL,
  `size` BIGINT(16) NOT NULL,
  `real_name` VARCHAR(255) NOT NULL,
  `remark` VARCHAR(512) DEFAULT NULL,
  `create_datetime` DATETIME NOT NULL,
  `modify_datetime` DATETIME NOT NULL,
  PRIMARY KEY(`id`),
  KEY index_0(`name`)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `article`;
CREATE TABLE `article`(
    `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
    `webapp_id` BIGINT(16) NOT NULL,
    `type_id` BIGINT(16) NOT NULL,
    `user_id` BIGINT(16) NOT NULL,
    `title` VARCHAR(512),
    `content` TEXT,
    `tags` VARCHAR(256) DEFAULT '' COMMENT ',分割',
    `click` BIGINT(16) DEFAULT 0,
    `assess` BIGINT(16) DEFAULT 0,
    `is_save` VARCHAR(2) DEFAULT 'F',
    `is_halt` VARCHAR(2) DEFAULT 'F',
    `create_datetime` DATETIME,
    `modify_datetime` DATETIME,
    PRIMARY KEY (`id`)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `article_type`;
CREATE TABLE `article_type`(
    `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
    `webapp_id` BIGINT(16) NOT NULL,
    `name` VARCHAR(64) NOT NULL,
    `is_halt` VARCHAR(2) DEFAULT 'F',
    `create_datetime` DATETIME,
    `modify_datetime` DATETIME,
    PRIMARY KEY (`id`),
    UNIQUE KEY `index_0`(`webapp_id`,`name`)
);

DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag` (
  `id` bigint(16) NOT NULL AUTO_INCREMENT,
  `webapp_id` BIGINT(16) NOT NULL,
  `name` varchar(32) NOT NULL,
  `size` BIGINT(16) DEFAULT 0,
  `is_halt` varchar(2) DEFAULT 'F',
  `create_datetime` datetime DEFAULT NULL,
  `modify_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_0`(`webapp_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `article_tag`;
CREATE TABLE `article_tag` (
  `id` bigint(16) NOT NULL AUTO_INCREMENT,
  `webapp_id` BIGINT(16) NOT NULL,
  `article_id` BIGINT(16) NOT NULL,
  `tag_id` BIGINT(16) NOT NULL,
  `is_halt` varchar(2) DEFAULT 'F',
  `create_datetime` datetime DEFAULT NULL,
  `modify_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_0`(`webapp_id`,`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ly`;
CREATE TABLE `ly` (
  `id` bigint(16) NOT NULL AUTO_INCREMENT,
  `webapp_id` BIGINT(16) NOT NULL,
  `article_id` BIGINT(16),
  `ly_id` BIGINT(16),
  `user_id` BIGINT(16),
  `img` VARCHAR(256),
  `author` varchar(256) NOT NULL,
  `email` varchar(256),
  `url` VARCHAR(256),
  `content` TEXT,
  `ip` varchar(16),
  `ip_name` varchar(512),
  `is_halt` varchar(2) DEFAULT 'F',
  `create_datetime` datetime DEFAULT NULL,
  `modify_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `link`;
CREATE TABLE `link` (
  `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
  `webapp_id` BIGINT(16) NOT NULL,
  `uid` VARCHAR(64) NOT NULL,
  `name` VARCHAR(256) NOT NULL,
  `url` varchar(256) NOT NULL,
  `remark` TEXT,
  `is_halt` varchar(2) DEFAULT 'F',
  `fr` BIGINT(16) DEFAULT 0,
  `to` BIGINT(16) DEFAULT 0,
  `create_datetime` datetime DEFAULT NULL,
  `modify_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_0`(`webapp_id`,`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ip`;
CREATE TABLE `ip` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `start` VARCHAR(16) DEFAULT NULL,
  `end` VARCHAR(16) DEFAULT NULL,
  `name` VARCHAR(512) DEFAULT NULL,
  `start_num` BIGINT(16) DEFAULT NULL,
  `end_num` BIGINT(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_0` (`start_num`,`end_num`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;